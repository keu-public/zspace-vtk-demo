## zSpace VTK demo

This repository contains a simple demo showing how we can build a VTK pipeline to render and interact with some actors on a zSpace device (windows only).

### How to build the demo

**Prerequisites**
- VTK is already built with the zSpace module enabled
- zSpace SDK is available on the macine

**Instructions**
- Open a new terminal.
- Go to your build location, for example:  
`cd path\to\the\demo\folder\Demo\build` 
- If the path to the zSpaceSDK is not added to your PATH environment variable, type:  
`set PATH=%PATH%;path\to\the\zspace\sdk\bin`  
in order to find the shared libraries during the build process.
- Run CMake. The VTK_DIR variable should point to your VTK build directory. For example:  
`cmake -GNinja -DVTK_DIR=path\to\vtk\build\directory ..`
- Build the demo:  
`ninja`
- If not already done, add the path to the VTK shared libraries to yout PATH environment variable:  
`set PATH=%PATH%;path\to\vtk\build\directory\bin`
- Launch the demo:  
`.\zSpaceDemo.exe`

### License

This repository is under the Apache 2.0 license, see NOTICE and LICENSE file.
