#include <vtkActor.h>
#include <vtkCommand.h>
#include <vtkConeSource.h>
#include <vtkDataObject.h>
#include <vtkNew.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderWindow.h>
#include <vtkSphereSource.h>

// ZSpace specific includes
#include <vtkZSpaceCamera.h>
#include <vtkZSpaceInteractorStyle.h>
#include <vtkZSpaceRayActor.h>
#include <vtkZSpaceRenderer.h>
#include <vtkZSpaceRenderWindowInteractor.h>
#include <vtkZSpaceSDKManager.h>

//------------------------------------------------------------------------------
int main(int, char*[])
{
  // Create a sphere source
  vtkNew<vtkSphereSource> sphereSource;
  sphereSource->SetThetaResolution(16);
  sphereSource->SetPhiResolution(16);
  sphereSource->Update();

  // Create a cone source
  vtkNew<vtkConeSource> coneSource;
  coneSource->SetResolution(16);
  coneSource->Update();

  // Connect each source to respective mappers
  vtkNew<vtkPolyDataMapper> mapper;
  mapper->SetInputConnection(sphereSource->GetOutputPort());

  vtkNew<vtkPolyDataMapper> mapper2;
  mapper2->SetInputConnection(coneSource->GetOutputPort());

  // Create an actor for each mapper
  vtkNew<vtkActor> actor;
  actor->SetMapper(mapper);
  actor->GetProperty()->SetRepresentation(VTK_SURFACE);
  actor->GetProperty()->SetEdgeVisibility(true);

  vtkNew<vtkActor> actor2;
  actor2->SetMapper(mapper2);
  actor2->GetProperty()->SetRepresentation(VTK_SURFACE);
  actor2->GetProperty()->SetEdgeVisibility(true);

  // [ZSPACE] Create the zSpace ray actor
  vtkNew<vtkZSpaceRayActor> rayActor;

  // [ZSPACE] Create the zSpace camera
  vtkNew<vtkZSpaceCamera> camera;

  // [ZSPACE] Create and setup the zSpace renderer
  // And add all the actors to the renderer
  vtkNew<vtkZSpaceRenderer> renderer;
  renderer->AddActor(actor);
  renderer->AddActor(actor2);
  renderer->AddActor(rayActor);
  renderer->SetActiveCamera(camera);
  renderer->SetBackground(0.2, 0.3, 0.4);

  // Setup the stereo render window
  // For zSpace (and quad buffer stereo in general),
  // the stereo type to set is "Crystal Eyes"
  vtkNew<vtkRenderWindow> renderWindow;
  renderWindow->AddRenderer(renderer);
  renderWindow->SetStereoCapableWindow(true);
  renderWindow->SetStereoRender(true);
  renderWindow->SetStereoTypeToCrystalEyes();
  renderWindow->SetSize(1600, 900); // Select resolution here
  // renderWindow->SetBorders(false);

  // [ZSPACE] Setup the zSpace render window interactor
  vtkNew<vtkZSpaceRenderWindowInteractor> renderWindowInteractor;
  renderWindowInteractor->SetRenderWindow(renderWindow);

  // [ZSPACE] Setup the zSpace interactor style 
  // (used to handle the stylus actions)
  vtkZSpaceInteractorStyle* interactorStyle = 
	  vtkZSpaceInteractorStyle::SafeDownCast(renderWindowInteractor->GetInteractorStyle());
  interactorStyle->SetZSpaceRayActor(rayActor);
  interactorStyle->SetCurrentRenderer(renderer);
  interactorStyle->SetPickingFieldAssociation(vtkDataObject::FIELD_ASSOCIATION_CELLS);

  // [ZSPACE] Setup the ZSpace Manager
  // This class communicates with the zSpace API and initialize
  // all needed variables. 
  vtkZSpaceSDKManager* manager = vtkZSpaceSDKManager::GetInstance();
  manager->SetRenderWindow(renderWindow);
  manager->Update();

  // Initialize the camera postion to fit the actors to the
  // comfort zone of the stereo frustum
  renderer->ResetCamera(actor->GetBounds());

  // Start the rendering and event loop
  renderWindow->Render();
  renderWindowInteractor->Start();

  return EXIT_SUCCESS;
}